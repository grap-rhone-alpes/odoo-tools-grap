# Developpment

## Basic installation

```
git clone https://gitlab.com/odoo-tools-grap/odoo-tools-grap/
cd odoo-tools-grap
virtualenv env --python=python3.9
. ./env/bin/activate
poetry install
```

``odoo-tools-grap`` and ``otg`` commands are now available in your virutalenv.

# Contribute

## Add python dependencies

If you add new dependencies, you have to:

- add the reference in the file ``pyproject.toml``

- run the following command in your virtualenv : ``poetry update``

# Publish

## First time

``poetry config pypi-token.pypi YOUR-PYPI-TOKEN``

## Each time

```
poetry build
poetry publish --skip-existing
```

# Run tests

TODO
