"""Executed when module is run as a script."""

from odoo_tools_grap import cli

cli.main()  # pylint: disable=no-value-for-parameter
